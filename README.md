# WebQuake

**WebQuake** is an HTML5 WebGL port of the game Quake by id Software.

Original conversion credit by https://github.com/Triang3l/WebQuake

# About

The original conversion had two source trees - one for Client and one for Server. This package contains a slightly refactored version of the original source with Typescript bindings (Still a gigantic TODO wrt types).  YOu may use this module in your javascript project to host the quake engine.

This module is used to power the quake engine on http://www.netquake.io

# Installing

`npm install webquake`

# Using

The entrypoint is `init`. This requires an object that implements platform specific functions. You must implement the gameloop.
